//
//  GameScene.h
//  zombies
//

//  Copyright (c) 2016 Corey Hom. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface GameScene : SKScene
{
    
    CGFloat gameScreenHeight;
    CGFloat gameScreenWidth;
}

@end