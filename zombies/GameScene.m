//
//  GameScene.m
//  zombies
//
//  Created by Corey on 4/26/16.
//  Copyright (c) 2016 Corey Hom. All rights reserved.
//

#import "GameScene.h"

@implementation GameScene
UIView *gunBar;
UIView *slider;
UILabel *clipDisplay;
SKSpriteNode *zombie1;
SKSpriteNode *zombie2;

int zombie1hp;
int zombie2hp;

SKSpriteNode *heart1;
SKSpriteNode *heart2;
SKSpriteNode *heart3;

SKSpriteNode *bullet1;
SKSpriteNode *bullet2;
SKSpriteNode *bullet3;
SKSpriteNode *bullet4;
SKSpriteNode *bullet5;
SKSpriteNode *bullet6;
SKSpriteNode *bullet7;

int clipCount;
int bulletCount;
int totalHealth;
int killCount;

-(void)didMoveToView:(SKView *)view {
    //Set up the window!
    gameScreenHeight = CGRectGetHeight(self.scene.view.bounds);
    gameScreenWidth = CGRectGetWidth(self.scene.view.bounds);
    
    bulletCount = 7;
    killCount = 0;
    clipCount = 1;
    
    gunBar  = [[UIView alloc] initWithFrame:CGRectMake(0, gameScreenHeight/1.25, gameScreenWidth, gameScreenHeight/20)];
    gunBar.backgroundColor = [UIColor blackColor];
    [self.view addSubview:gunBar];
    
    slider = [[UIView alloc] initWithFrame: CGRectMake(0, gameScreenHeight/1.26, gameScreenWidth/20, gameScreenHeight/15)];
    slider.layer.borderWidth = 1;
    slider.layer.borderColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:1.0].CGColor;
    slider.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:slider];
    
    
    
    [UIView animateWithDuration:2.5f
                          delay:0.0f
                        options:UIViewAnimationOptionRepeat | UIViewAnimationOptionAutoreverse
                     animations:^{
                         [slider setFrame:CGRectMake(gameScreenWidth - gameScreenWidth/20, gameScreenHeight/1.26, gameScreenWidth/20, gameScreenHeight/15)];
                     }
                     completion:nil];
    
    UIButton *fireButton = [UIButton buttonWithType:UIButtonTypeSystem];
    fireButton.frame = CGRectMake(gameScreenWidth/2.5,gameScreenHeight/1.2,100,100);
    [fireButton setBackgroundImage:[UIImage imageNamed:@"button"] forState:UIControlStateNormal];
    [fireButton addTarget:self action:@selector(fireClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:fireButton];
    
    UIButton *reloadButton = [UIButton buttonWithType:UIButtonTypeSystem];
    reloadButton.frame = CGRectMake(gameScreenWidth/2.5 - 50, gameScreenHeight/1.2 + 25,50,50);
    [reloadButton setBackgroundImage:[UIImage imageNamed:@"reload"] forState:UIControlStateNormal];
    [reloadButton addTarget:self action:@selector(reloadClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:reloadButton];
    
    UILabel* clipLabel = [[UILabel alloc] initWithFrame: CGRectMake(gameScreenWidth/2.5 - 115, gameScreenHeight/1.25 + 25, 50,50)];
    clipLabel.text = @"Clips: ";
    [self.view addSubview: clipLabel];
    clipDisplay = [[UILabel alloc] initWithFrame: CGRectMake(gameScreenWidth/2.5 - 100, gameScreenHeight/1.2 + 25, 50, 50)];
    [clipDisplay setText:[NSString stringWithFormat:@"%d", clipCount]];
    [self.view addSubview:clipDisplay];
    
    zombie1 = [SKSpriteNode spriteNodeWithTexture:[SKTexture textureWithImageNamed:@"zombie"] size:CGSizeMake(200, 200)];
    zombie1.alpha = 1.0;
    zombie1.position = CGPointMake(gameScreenHeight/1.5, gameScreenWidth/1.5);
    zombie1hp = 3;
    zombie1.zPosition = 2;
    
    [self addChild: zombie1];
    
    
    
    zombie2 = [SKSpriteNode spriteNodeWithTexture:[SKTexture textureWithImageNamed:@"zombie"] size:CGSizeMake(150, 150)];
    zombie2.alpha = 1.0;
    zombie2.position = CGPointMake(gameScreenHeight/2.0, gameScreenWidth/1.5);
    zombie2hp = 3;
    zombie2.zPosition = 1;
    [self addChild: zombie2];

    
    totalHealth = 3;
    heart1 = [SKSpriteNode spriteNodeWithTexture:[SKTexture textureWithImageNamed:@"heart"] size:CGSizeMake(50, 50)];
    heart2 = [SKSpriteNode spriteNodeWithTexture:[SKTexture textureWithImageNamed:@"heart"] size:CGSizeMake(50, 50)];
    heart3 = [SKSpriteNode spriteNodeWithTexture:[SKTexture textureWithImageNamed:@"heart"] size:CGSizeMake(50, 50)];
    
    heart1.position = CGPointMake(gameScreenHeight/1.1, gameScreenWidth/15);
    heart2.position = CGPointMake(gameScreenHeight/1.1 - 50, gameScreenWidth/15);
    heart3.position = CGPointMake(gameScreenHeight/1.1 - 100,gameScreenWidth/15);
    
    [self addChild: heart1];
    [self addChild: heart2];
    [self addChild: heart3];
    
    bullet1 = [SKSpriteNode spriteNodeWithTexture: [SKTexture textureWithImageNamed:@"bullet"] size: CGSizeMake(25, 25)];
    bullet2 = [SKSpriteNode spriteNodeWithTexture: [SKTexture textureWithImageNamed:@"bullet"] size: CGSizeMake(25, 25)];
    bullet3 = [SKSpriteNode spriteNodeWithTexture: [SKTexture textureWithImageNamed:@"bullet"] size: CGSizeMake(25, 25)];
    bullet4 = [SKSpriteNode spriteNodeWithTexture: [SKTexture textureWithImageNamed:@"bullet"] size: CGSizeMake(25, 25)];
    bullet5 = [SKSpriteNode spriteNodeWithTexture: [SKTexture textureWithImageNamed:@"bullet"] size: CGSizeMake(25, 25)];
    bullet6 = [SKSpriteNode spriteNodeWithTexture: [SKTexture textureWithImageNamed:@"bullet"] size: CGSizeMake(25, 25)];
    bullet7 = [SKSpriteNode spriteNodeWithTexture: [SKTexture textureWithImageNamed:@"bullet"] size: CGSizeMake(25, 25)];
    
    bullet1.position = CGPointMake(gameScreenHeight/2.4, gameScreenWidth/15);
    bullet2.position = CGPointMake(gameScreenHeight/2.4 + 25, gameScreenWidth/15);
    bullet3.position = CGPointMake(gameScreenHeight/2.4 + 50, gameScreenWidth/15);
    bullet4.position = CGPointMake(gameScreenHeight/2.4 + 75, gameScreenWidth/15);
    bullet5.position = CGPointMake(gameScreenHeight/2.4 + 100, gameScreenWidth/15);
    bullet6.position = CGPointMake(gameScreenHeight/2.4 + 125, gameScreenWidth/15);
    bullet7.position = CGPointMake(gameScreenHeight/2.4 + 150, gameScreenWidth/15);
    
    [self addChild: bullet1];
    [self addChild: bullet2];
    [self addChild: bullet3];
    [self addChild: bullet4];
    [self addChild: bullet5];
    [self addChild: bullet6];
    [self addChild: bullet7];
}

-(void) reloadClicked
{
    if (clipCount > 0){
        if (bullet7.hidden == YES)
        {
            bullet7.hidden = NO;
        }
        if (bullet6.hidden == YES)
        {
            bullet6.hidden = NO;
        }
        if (bullet5.hidden == YES)
        {
            bullet5.hidden = NO;
        }
        if (bullet4.hidden == YES)
        {
            bullet4.hidden = NO;
        }
        if (bullet3.hidden == YES)
        {
            bullet3.hidden = NO;
        }
        if (bullet2.hidden == YES)
        {
            bullet2.hidden = NO;
        }
        if (bullet1.hidden == YES)
        {
            bullet1.hidden = NO;
        }
        bulletCount = 7;
        clipCount --;
        [clipDisplay setText:[NSString stringWithFormat:@"%d", clipCount]];
    }
}

-(void) fireClicked
{
    bulletCount--;
    if (bulletCount == 6)
    {
        bullet7.hidden = YES;
    }
    if (bulletCount == 5)
    {
        bullet6.hidden = YES;
    }
    if (bulletCount == 4)
    {
        bullet5.hidden = YES;
    }
    if (bulletCount == 3)
    {
        bullet4.hidden = YES;
    }
    if (bulletCount == 2)
    {
        bullet3.hidden = YES;
    }
    if (bulletCount == 1)
    {
        bullet2.hidden = YES;
    }
    if (bulletCount == 0)
    {
        bullet1.hidden = YES;
    }
    
    if (bulletCount >= 0){
        SKAction *moveRight  = [SKAction moveToX:650 duration:4];
        SKAction *moveLeft   = [SKAction moveToX:350 duration:4];
        
        SKAction *moveRight2  = [SKAction moveToX:600 duration:4];
        SKAction *moveLeft2   = [SKAction moveToX:350 duration:4];
        CGRect projectileFrame = [[slider.layer presentationLayer] frame];
        NSLog(@"%f", projectileFrame.origin.x);
        NSLog(@"%f", zombie1.position.x);
        NSLog(@"%f", zombie1.position.y);
        NSLog(@"Shot fired.");
        if (projectileFrame.origin.x < zombie1.position.x - 280 && projectileFrame.origin.x > zombie1.position.x - 350 && zombie1.hidden == NO)
        {
            NSLog(@"hit1");
            SKAction *blink = [SKAction sequence:@[[SKAction fadeOutWithDuration:0.1],
                                                   [SKAction fadeInWithDuration:0.1]]];
            SKAction *blinkForTime = [SKAction repeatAction:blink count:4];
            [zombie1 runAction:blinkForTime];
            zombie1hp--;
            [zombie1 runAction: moveRight];
            if (zombie1hp == 0)
            {
                zombie1.hidden = YES;
                killCount++;
            }
            if (zombie1hp == 1)
            {
                [zombie1 runAction: moveLeft];
            }
            if (zombie1hp == 2){
                [zombie1 runAction: moveRight];
            }
            
        }
        else if (projectileFrame.origin.x < zombie2.position.x - 280 && projectileFrame.origin.x > zombie2.position.x - 350 && zombie2.hidden == NO)
        {
            NSLog(@"hit2");
            SKAction *blink = [SKAction sequence:@[[SKAction fadeOutWithDuration:0.1],
                                                   [SKAction fadeInWithDuration:0.1]]];
            SKAction *blinkForTime = [SKAction repeatAction:blink count:4];
            [zombie2 runAction:blinkForTime];
            zombie2hp--;
            if (zombie2hp == 0)
            {
                zombie2.hidden = YES;
                killCount++;
            }
            if (zombie2hp == 1)
            {
                [zombie2 runAction: moveLeft2];
            }
            if (zombie2hp == 2){
                [zombie2 runAction: moveRight2];
            }
        }
        else
        {
            totalHealth--;
            if (totalHealth == 2)
            {
                heart3.hidden = YES;
            }
            if (totalHealth == 1)
            {
                heart2.hidden = YES;
            }
        }
    }
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {

}

-(void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
    if (zombie1hp <= 0 && zombie2hp <= 0)
    {
        zombie1.hidden = NO;
        zombie2.hidden = NO;
        
        zombie1hp = 3;
        zombie2hp = 3;
        clipCount++;
        [clipDisplay setText:[NSString stringWithFormat:@"%d", clipCount]];
        
    }
    if (bulletCount <= 0 && clipCount <= 0)
    {
        NSString *scoreOutput = [NSString stringWithFormat: @"Game over! You killed %d zombies.", killCount];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"You have nothing left! The zombies swarm you and you die." message:scoreOutput delegate:nil cancelButtonTitle:@"New Game" otherButtonTitles:nil];
        [alert show];
        killCount = 0;
        totalHealth = 3;
        heart1.hidden = NO;
        heart2.hidden = NO;
        heart3.hidden = NO;
        zombie1.hidden = NO;
        zombie2.hidden = NO;
        
        bullet7.hidden = NO;
        bullet6.hidden = NO;
        bullet5.hidden = NO;
        bullet4.hidden = NO;
        bullet3.hidden = NO;
        bullet2.hidden = NO;
        bullet1.hidden = NO;
        
        zombie1hp = 3;
        zombie2hp = 3;
        clipCount = 1;
        bulletCount = 7;
        [clipDisplay setText:[NSString stringWithFormat:@"%d", clipCount]];
    }
    
    if (totalHealth == 0)
    {
        NSString *scoreOutput = [NSString stringWithFormat: @"Game over! You killed %d zombies.", killCount];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Your health is 0! You died." message:scoreOutput delegate:nil cancelButtonTitle:@"New Game" otherButtonTitles:nil];
        [alert show];
        killCount = 0;
        totalHealth = 3;
        heart1.hidden = NO;
        heart2.hidden = NO;
        heart3.hidden = NO;
        zombie1.hidden = NO;
        zombie2.hidden = NO;
        
        bullet7.hidden = NO;
        bullet6.hidden = NO;
        bullet5.hidden = NO;
        bullet4.hidden = NO;
        bullet3.hidden = NO;
        bullet2.hidden = NO;
        bullet1.hidden = NO;
        
        zombie1hp = 3;
        zombie2hp = 3;
        clipCount = 1;
        bulletCount = 7;
        [clipDisplay setText:[NSString stringWithFormat:@"%d", clipCount]];
    }
}

@end
