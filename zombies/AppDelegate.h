//
//  AppDelegate.h
//  zombies
//
//  Created by Corey Hom on 4/26/16.
//  Copyright © 2016 Corey Hom. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

